﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MyGames.Domain.Interfaces.UnitOfWork;
using MyGames.Infrastructure.Data.Contexts;
using MyGames.Infrastructure.Data.Repositories;

namespace MyGames.Infrastructure.Data
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DbContext, AppDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUnitOfWorkQuery, UnitOfWorkQuery>();

            return services;
        }
    }
}