﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyGames.Infrastructure.Data.Migrations
{
    public partial class Title : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NormalizedName",
                schema: "Games",
                table: "Studios",
                newName: "Title");

            migrationBuilder.RenameColumn(
                name: "NormalizedName",
                schema: "Games",
                table: "Genres",
                newName: "Title");

            migrationBuilder.RenameColumn(
                name: "NormalizedName",
                schema: "Games",
                table: "Games",
                newName: "Title");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Title",
                schema: "Games",
                table: "Studios",
                newName: "NormalizedName");

            migrationBuilder.RenameColumn(
                name: "Title",
                schema: "Games",
                table: "Genres",
                newName: "NormalizedName");

            migrationBuilder.RenameColumn(
                name: "Title",
                schema: "Games",
                table: "Games",
                newName: "NormalizedName");
        }
    }
}
