﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using MyGames.Domain.Interfaces.Repositories;
using MyGames.Domain.Interfaces.UnitOfWork;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MyGames.Infrastructure.Data.Repositories
{
    /// <summary>
    /// unit of work for command (write-read)
    /// </summary>
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private IDbContextTransaction _transaction;
        private bool _disposed;

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        IRepository<T, TId> IUnitOfWork.Repository<T, TId>() => new Repository<T, TId>(_dbContext);

        async Task IUnitOfWork.SaveChangesAsync(CancellationToken ct = default)
        {
            await _dbContext.SaveChangesAsync(ct);
        }

        async Task IUnitOfWork.SaveChangesAndCommitTransactionAsync(CancellationToken ct = default)
        {
            try
            {
                await _dbContext.SaveChangesAsync(ct);
                await _transaction.CommitAsync(ct);
            }
            catch
            {
                await _transaction.RollbackAsync(ct);
                throw;
            }
        }

        async Task IUnitOfWork.BeginTransactionASync(CancellationToken ct = default)
        {
            _transaction ??= await _dbContext.Database.BeginTransactionAsync(ct);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            _disposed = true;
        }
    }
}