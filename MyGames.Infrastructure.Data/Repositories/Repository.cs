﻿using Microsoft.EntityFrameworkCore;
using MyGames.Domain.Core.Base;
using MyGames.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyGames.Infrastructure.Data.Repositories
{
    public class Repository<T, TId> : IRepository<T, TId> where T : Entity<TId>
                                                          where TId : IEquatable<TId>
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;

        public Repository(DbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = _context.Set<T>();
        }

        async Task<T> IRepository<T, TId>.FirstOrDefaultAsync(Expression<Func<T, bool>> predicate = default, IEnumerable<string>? properties = default)
            => await GetQuery(properties).FirstOrDefaultAsync(predicate);

        async Task<IReadOnlyList<T>> IRepository<T, TId>.GetAllAsync(IEnumerable<string>? properties = default)
            => await GetQuery(properties).ToListAsync();

        async Task<IReadOnlyList<T>> IRepository<T, TId>.FindByAsync(Expression<Func<T, bool>> predicate, IEnumerable<string>? properties = default)
            => await GetQuery(properties).Where(predicate).ToListAsync();

        async Task IRepository<T, TId>.AddAsync(T entity)
            => await _dbSet.AddAsync(entity);

        void IRepository<T, TId>.Delete(T entity)
        {
            if (entity.Delete())
                _dbSet.Update(entity);
        }
        void IRepository<T, TId>.Update(T entity) => _dbSet.Update(entity);

        private IQueryable<T> GetQuery(IEnumerable<string>? properties = default)
        {
            IQueryable<T> query = _dbSet.Where(entity => !entity.IsDeleted);

            if (properties is null || !properties.Any())
                return query;

            return properties.Aggregate(query, (current, property) => current.Include(property));
        }
    }
}