﻿using Microsoft.EntityFrameworkCore;
using MyGames.Domain.Core.Base;
using MyGames.Domain.Interfaces.Repositories;
using MyGames.Domain.Interfaces.UnitOfWork;
using System;

namespace MyGames.Infrastructure.Data.Repositories
{
    /// <summary>
    /// unit of work for queries (read-only)
    /// </summary>
    public sealed class UnitOfWorkQuery : IUnitOfWorkQuery
    {
        private readonly DbContext _dbContext;
        private bool _disposed;

        public UnitOfWorkQuery(DbContext dbContext) => _dbContext = dbContext;

        public IRepositoryQuery<T, TId> ReadOnlyRepository<T, TId>() where T : Entity<TId>
                                                        where TId : IEquatable<TId>
            => new RepositoryQuery<T, TId>(_dbContext);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            _disposed = true;
        }
    }
}