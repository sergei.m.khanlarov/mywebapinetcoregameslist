﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyGames.Domain.Core.GamesAggregate.Entities;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;

namespace MyGames.Infrastructure.Data.Configurations
{
    public class GenreConfiguration : IEntityTypeConfiguration<Genre>
    {
        public void Configure(EntityTypeBuilder<Genre> builder)
        {
            builder.ToTable("Genres", "Games");

            builder.HasKey(g => g.Id);

            builder.Property(g => g.Id)
                   .HasConversion(id => id.ToString(),
                                  value => GenreId.Parse(value));

            builder.Property(g => g.Title)
                .HasConversion(title => title.ToString(),
                                value => Title.Parse(value));

            builder.Property(g => g.CreatedBy);

            builder.Property(g => g.CreatedAt);

            builder.Ignore(g => g.ModifiedAt);

            builder.Ignore(g => g.ModifiedBy);

            builder.Property(g => g.IsDeleted);
        }
    }
}