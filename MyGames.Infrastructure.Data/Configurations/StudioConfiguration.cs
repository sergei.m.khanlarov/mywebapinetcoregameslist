﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyGames.Domain.Core.GamesAggregate.Entities;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;

namespace MyGames.Infrastructure.Data.Configurations
{
    internal class StudioConfiguration : IEntityTypeConfiguration<Studio>
    {
        public void Configure(EntityTypeBuilder<Studio> builder)
        {
            builder.ToTable("Studios", "Games");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Id)
                   .HasConversion(id => id.ToString(),
                                  value => StudioId.Parse(value));

            builder.Property(g => g.Title)
                .HasConversion(title => title.ToString(),
                                value => Title.Parse(value));

            builder.Property(s => s.CreatedBy);

            builder.Property(s => s.CreatedAt);

            builder.Ignore(s => s.ModifiedAt);

            builder.Ignore(s => s.ModifiedBy);

            builder.Property(s => s.IsDeleted);
        }
    }
}