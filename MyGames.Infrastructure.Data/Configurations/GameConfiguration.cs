﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using MyGames.Domain.Core.GamesAggregate;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;

namespace MyGames.Infrastructure.Data.Configurations
{
    internal class GameConfiguration : IEntityTypeConfiguration<Game>
    {
        public void Configure(EntityTypeBuilder<Game> builder)
        {
            builder.ToTable("Games", "Games");

            builder.HasKey(g => g.Id);

            builder.Property(g => g.Id)
                   .HasConversion(id => id.ToString(),
                                  value => GameId.Parse(value));

            builder.ToTable("Games", "Games");

            builder.Property(g => g.Title)
                .HasConversion(title => title.ToString(),
                                value => Title.Parse(value));

            builder.HasMany(g => g.Genres)
                .WithMany(g => g.Games)
                .UsingEntity(builder => builder.ToTable("GamesGenres", "Games"));

            builder.HasOne(g => g.Studio)
                .WithMany(g => g.Games);

            builder.Property(g => g.CreatedBy).HasComment("Has Created By");

            builder.Property(g => g.CreatedAt).HasComment("Created at");

            builder.Property(g => g.ModifiedAt);

            builder.Property(g => g.ModifiedBy);

            builder.Property(g => g.IsDeleted);
        }
    }
}