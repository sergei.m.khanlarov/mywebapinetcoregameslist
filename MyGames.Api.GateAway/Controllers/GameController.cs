﻿using FluentResults.Extensions.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyGames.Application.Dtos;
using MyGames.Application.Interfaces;
using MyGames.Domain.Dtos.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyGames.Api.GateAway.Controllers
{
    /// <summary>
    /// Контроллер для агрегата игры
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class GameController : Controller
    {
        private readonly IGameServiceBuilder _builder;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public GameController(IGameServiceBuilder builder)
        {
            if (builder is null)
                throw new ArgumentNullException(nameof(builder));

            _builder = builder;
        }

        /// <summary>
        /// Creates a game
        /// </summary>
        /// <param name="command">create command</param>
        /// <returns></returns>
        /// <response code="200">Returns the new item's identifier</response>
        /// <response code="400">If the item is null</response>
        [HttpPost]
        [Route("")]
        [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create([FromBody] CreateGameDto command)
            => (await _builder.CreateGameAsync(command)).ToActionResult();

        /// <summary>
        /// Updates a game
        /// </summary>
        /// <param name="gameId">game identifier</param>
        /// <param name="command">command with params</param>
        /// <returns></returns>
        /// <response code="200">updated item</response>
        /// <response code="400">If the item is null</response>
        [HttpPut]
        [Route("{gameId}")]
        [ProducesResponseType(typeof(GameViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update([FromRoute] string gameId, [FromBody] UpdateGameDto command)
            => (await _builder.UpdateGameAsync(gameId, command)).ToActionResult();

        /// <summary>
        /// Deletes a game
        /// </summary>
        /// <param name="gameId">identifier</param>
        /// <returns></returns>
        /// <response code="200">deleted item identifier</response>
        /// <response code="400">If the item is null</response>
        [HttpDelete]
        [Route("{gameId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete([FromRoute] string gameId)
            => (await _builder.DeleteGameAsync(gameId)).ToActionResult();

        /// <summary>
        /// Returns all games info
        /// </summary>
        /// <param name="search">search word</param>
        /// <param name="offset">offset</param>
        /// <param name="limit">limit</param>
        /// <returns></returns>
        /// <response code="200">Returns all games info</response>
        /// <response code="400">If no games records exist</response>
        [HttpGet]
        [Route("")]
        [ProducesResponseType(typeof(IReadOnlyCollection<GameViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get([FromQuery] string? search, [FromQuery] int? offset, [FromQuery] int? limit)
            => (await _builder.GetGamesAsync(new GetGamesDto(search, offset, limit))).ToActionResult();

        /// <summary>
        /// Returns a games info
        /// </summary>
        /// <param name="gameId">identifier</param>
        /// <returns></returns>
        /// <response code="200">Returns all games info</response>
        /// <response code="400">If no games records exist</response>
        [HttpGet]
        [Route("{gameId}")]
        [ProducesResponseType(typeof(GameViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get([FromRoute] string gameId)
            => (await _builder.GetGameAsync(gameId)).ToActionResult();
    }
}