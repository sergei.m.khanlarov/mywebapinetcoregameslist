using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyGames.Api.GateAway.Swagger;
using MyGames.Api.Services;

namespace MyGames.Api.GateAway
{
    public class Startup
    {
        private readonly IConfiguration _conf;

        public Startup(IConfiguration conf) => _conf = conf;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddServices(_conf);
            services.AddControllers();
            services.AddSwaggerCustom();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStaticFiles();
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();

            if (env.IsDevelopment())
            {
                app.UseSwaggerCustom();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}