﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MyGames.Api.GateAway.Swagger
{
    public static class SwaggerSettings
    {
        public static IServiceCollection AddSwaggerCustom(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                                   new OpenApiInfo
                                   {
                                       Version = "v1",
                                       Title = "My Game List API",
                                       Description = "Games infornation"
                                   });

                var xmlFilesNames = GetExistedXmlFiles();

                foreach (var xmlFile in xmlFilesNames)
                {
                    options.IncludeXmlComments(xmlFile, includeControllerXmlComments: true);
                }
            });

            return services;
        }

        public static IApplicationBuilder UseSwaggerCustom(this IApplicationBuilder app)
        {
            app.UseSwagger(options => { options.SerializeAsV2 = true; });

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                options.RoutePrefix = string.Empty;
            });

            return app;
        }

        private static IList<string> GetExistedXmlFiles()
        {
            var enrtyAssemby = Assembly.GetExecutingAssembly();

            var customReferenceAssemblies = enrtyAssemby.GetReferencedAssemblies().ToList();

            customReferenceAssemblies.Add(enrtyAssemby.GetName());

            return customReferenceAssemblies.Select(x => Path.Combine(AppContext.BaseDirectory, $"{x.Name}.xml"))
                .Where(File.Exists).ToList();
        }
    }
}