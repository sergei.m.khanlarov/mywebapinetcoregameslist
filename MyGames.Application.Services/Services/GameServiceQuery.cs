﻿using AutoMapper;
using FluentResults;
using MyGames.Application.Services.Constants;
using MyGames.Application.Services.Extensions;
using MyGames.Application.Services.FluentErrors;
using MyGames.Domain.Core.GamesAggregate;
using MyGames.Domain.Core.GamesAggregate.Entities;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;
using MyGames.Domain.Dtos.Queries;
using MyGames.Domain.Dtos.ViewModels;
using MyGames.Domain.Interfaces.Repositories;
using MyGames.Domain.Interfaces.Services;
using MyGames.Domain.Interfaces.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyGames.Application.Services.Services
{
    /// <summary>
    /// query service
    /// </summary>
    public class GameServiceQuery : IGameServiceQuery
    {
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly IRepositoryQuery<Game, GameId> _gameReadOnlyRepositoryQuery;

        public GameServiceQuery(IUnitOfWorkQuery unitOfWorkQuery,
                                ILogger logger,
                                IMapper mapper)
        {
            _gameReadOnlyRepositoryQuery = unitOfWorkQuery.ReadOnlyRepository<Game, GameId>(); ;
            _mapper = mapper;
            _logger = logger;
        }

        async Task<Result<GameViewModel>> IGameServiceQuery.GetGameAsync(GetGameQuery query)
        {
            try
            {
                _logger.Information(AppConstants.MethodStarted(nameof(IGameServiceQuery.GetGameAsync)));

                Game game = await _gameReadOnlyRepositoryQuery.FirstOrDefaultAsync(game => game.Id.Equals(GameId.Parse(query.Id)), new[] { nameof(Game.Genres), nameof(Game.Studio) });

                if (game is null)
                    return Result.Fail(NotFoundError.Entity(nameof(Genre), query.Id)).LogWarning(_logger);

                return _mapper.Map<GameViewModel>(game);
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError
                    .New(AppConstants.MethodFailed(nameof(IGameServiceQuery.GetGameAsync), ex.Message)))
                    .LogError(ex, _logger);
            }
        }

        async Task<Result<IReadOnlyCollection<GameViewModel>>> IGameServiceQuery.GetGamesAsync(GetGamesQuery query)
        {
            try
            {
                _logger.Information(AppConstants.MethodStarted(nameof(IGameServiceQuery.GetGamesAsync)));

                if (!string.IsNullOrWhiteSpace(query.Search))
                    return _mapper.Map<IReadOnlyCollection<Game>, IReadOnlyCollection<GameViewModel>>((
                        await _gameReadOnlyRepositoryQuery.GetAllAsync(new[] { nameof(Game.Genres), nameof(Game.Studio) }, query.Offset ?? 0, query.Limit ?? 10))
                        .Where(game => game.Genres.Any(genre => genre.Title.ToString().Contains(query.Search))).ToList()).ToList();

                return _mapper.Map<IReadOnlyCollection<Game>, IReadOnlyCollection<GameViewModel>>(
                    await _gameReadOnlyRepositoryQuery.GetAllAsync(new[] { nameof(Game.Genres), nameof(Game.Studio) }, query.Offset ?? 0, query.Limit ?? 10)).ToList();
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError
                    .New(AppConstants.MethodFailed(nameof(IGameServiceQuery.GetGamesAsync), ex.Message)))
                    .LogError(ex, _logger);
            }
        }
    }
}