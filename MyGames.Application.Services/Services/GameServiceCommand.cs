﻿using AutoMapper;
using FluentResults;
using MyGames.Application.Services.Constants;
using MyGames.Application.Services.Extensions;
using MyGames.Application.Services.FluentErrors;
using MyGames.Domain.Core.GamesAggregate;
using MyGames.Domain.Core.GamesAggregate.Entities;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;
using MyGames.Domain.Dtos.Commands;
using MyGames.Domain.Dtos.ViewModels;
using MyGames.Domain.Interfaces.Repositories;
using MyGames.Domain.Interfaces.Services;
using MyGames.Domain.Interfaces.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyGames.Application.Services.Services
{
    /// <summary>
    /// command service
    /// </summary>
    public class GameServiceCommand : IGameServiceCommand
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        private readonly IRepository<Game, GameId> _gameRepository;
        private readonly IRepository<Genre, GenreId> _genreRepository;
        private readonly IRepository<Studio, StudioId> _studioRepository;
        private readonly IRepositoryQuery<Genre, GenreId> _genreRepositoryReadOnly;
        private readonly IRepositoryQuery<Studio, StudioId> _studioRepositoryReadOnly;

        public GameServiceCommand(IUnitOfWork unitOfWork,
                                IUnitOfWorkQuery unitOfWorkQuery,
                                ILogger logger,
                                IMapper mapper)
        {
            _genreRepositoryReadOnly = unitOfWorkQuery.ReadOnlyRepository<Genre, GenreId>();
            _studioRepositoryReadOnly = unitOfWorkQuery.ReadOnlyRepository<Studio, StudioId>();
            _unitOfWork = unitOfWork;
            _gameRepository = unitOfWork.Repository<Game, GameId>();
            _genreRepository = unitOfWork.Repository<Genre, GenreId>();
            _studioRepository = unitOfWork.Repository<Studio, StudioId>();
            _mapper = mapper;
            _logger = logger.ForContext<GameServiceCommand>();
        }

        async Task<Result<string>> IGameServiceCommand.CreateGameAsync(CreateGameCommand command)
        {
            try
            {
                _logger.Information(AppConstants.MethodStarted(nameof(IGameServiceCommand.CreateGameAsync)));

                await _unitOfWork.BeginTransactionASync();

                Result<Studio> getStudioResult = await GetStudioByTitleAsync(command.Studio);

                if (getStudioResult.IsFailed)
                    return getStudioResult.ToResult().LogWarning(_logger);

                Result<ISet<Genre>> getGenreResult = await GetGenresByTitlesAsync(command.Genres);

                if (getGenreResult.IsFailed)
                    return getGenreResult.ToResult().LogWarning(_logger);

                Game game = new Game(GameId.New(), Title.Parse(command.Title), getGenreResult.ValueOrDefault, getStudioResult.ValueOrDefault);

                await _gameRepository.AddAsync(game);

                await _unitOfWork.SaveChangesAndCommitTransactionAsync();

                return game.Id.ToString();
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError
                    .New(AppConstants.MethodFailed(nameof(IGameServiceCommand.CreateGameAsync), ex.Message)))
                    .LogError(ex, _logger);
            }
        }

        async Task<Result<GameViewModel>> IGameServiceCommand.UpdateGameAsync(UpdateGameCommand command)
        {
            try
            {
                _logger.Information(AppConstants.MethodStarted(nameof(IGameServiceCommand.UpdateGameAsync)));

                await _unitOfWork.BeginTransactionASync();

                Result<Game> gameResult = await GetGameById(command.Id);

                if (gameResult.IsFailed)
                    return gameResult.ToResult();

                Result<Studio> getStudioResult = await GetStudioByTitleAsync(command.Studio);

                if (getStudioResult.IsFailed)
                    return getStudioResult.ToResult().LogWarning(_logger);

                Result<ISet<Genre>> getGenreResult = await GetGenresByTitlesAsync(command.Genres);

                if (getGenreResult.IsFailed)
                    return getGenreResult.ToResult().LogWarning(_logger);

                Game game = gameResult.ValueOrDefault;

                game.Update(Title.Parse(command.Title), getGenreResult.ValueOrDefault, getStudioResult.ValueOrDefault);

                await _unitOfWork.SaveChangesAndCommitTransactionAsync();

                return _mapper.Map<GameViewModel>(game);
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError
                    .New(AppConstants.MethodFailed(nameof(IGameServiceCommand.UpdateGameAsync), ex.Message)))
                    .LogError(ex, _logger);
            }
        }

        async Task<Result> IGameServiceCommand.DeleteGameAsync(DeleteGameCommand command)
        {
            try
            {
                _logger.Information(AppConstants.MethodStarted(nameof(IGameServiceCommand.DeleteGameAsync)));

                var getGameResult = await GetGameById(command.Id);

                if (getGameResult.IsFailed)
                    return getGameResult.ToResult().LogWarning(_logger);

                _gameRepository.Delete(getGameResult.ValueOrDefault);

                await _unitOfWork.SaveChangesAsync();

                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError
                    .New(AppConstants.MethodFailed(nameof(IGameServiceCommand.DeleteGameAsync), ex.Message)))
                    .LogError(ex, _logger);
            }
        }

        #region Game

        private async Task<Result<Game>> GetGameById(string Id)
        {
            _logger.Information(AppConstants.MethodStarted(nameof(GetGenreByTitleAsync)));

            GameId gameId = GameId.Parse(Id);

            Game game = await _gameRepository.FirstOrDefaultAsync(g => g.Id.Equals(gameId),
                                                            new[]
                                                            {
                                                                nameof(Game.Genres), nameof(Game.Studio)
                                                            });
            if (game is null)
                return Result.Fail(NotFoundError.Entity(nameof(Game), Id));

            return game;
        }

        #endregion

        #region Studio

        private async Task<Result<Studio>> GetStudioByTitleAsync(string title)
        {
            _logger.Information(AppConstants.MethodStarted(nameof(GetGenreByTitleAsync)));

            Title titleVal = Title.Parse(title);

            Result<Studio> getStudioResult = await GetStudioByTitleAsync(titleVal);

            if (getStudioResult.IsSuccess)
                return getStudioResult;

            Result<Studio> createStudioResult = await CreateStudioByTitleAsync(titleVal);

            return createStudioResult;
        }

        private async Task<Result<Studio>> GetStudioByTitleAsync(Title title)
        {
            _logger.Information(AppConstants.MethodStarted(nameof(GetGenreByTitleAsync)));

            Studio studio = await _studioRepositoryReadOnly.FirstOrDefaultAsync(studio => studio.Title.Equals(title));

            if (studio is null)
                return Result.Fail(NotFoundError.Entity(nameof(Studio)));

            return studio;
        }

        private async Task<Result<Studio>> CreateStudioByTitleAsync(Title title)
        {
            try
            {
                _logger.Information(AppConstants.MethodStarted(nameof(GetGenreByTitleAsync)));

                Studio studio = new Studio(StudioId.New(), title);

                await _studioRepository.AddAsync(studio);

                await _unitOfWork.SaveChangesAsync();

                return studio;
            }
            catch (Exception ex)
            {
                _logger.Error(AppConstants.MethodFailed(nameof(CreateGenreByTitleAsync), ex.Message), ex, ex.InnerException);
                return Result.Fail(InternarServerError.New(AppConstants.MethodFailed(nameof(CreateGenreByTitleAsync), ex.Message)));
            }
        }

        #endregion

        #region Genre

        private async Task<Result<ISet<Genre>>> GetGenresByTitlesAsync(IEnumerable<string> titles)
        {
            _logger.Information(AppConstants.MethodStarted(nameof(GetGenresByTitlesAsync)));

            HashSet<Genre> genres = new HashSet<Genre>();

            foreach (string title in titles)
            {

                Result<Genre> getGenreResult = await GetGenreByTitleAsync(title);

                if (getGenreResult.IsFailed)
                    return getGenreResult.ToResult();

                genres.Add(getGenreResult.ValueOrDefault);
            }

            return genres;
        }

        private async Task<Result<Genre>> GetGenreByTitleAsync(string title)
        {
            _logger.Information(AppConstants.MethodStarted(nameof(GetGenreByTitleAsync)));

            Title titleVal = Title.Parse(title);

            Result<Genre> getGenreResult = await GetGenreByTitleAsync(titleVal);

            if (getGenreResult.IsSuccess)
                return getGenreResult;

            Result<Genre> createGenreResult = await CreateGenreByTitleAsync(titleVal);

            return createGenreResult;
        }

        private async Task<Result<Genre>> GetGenreByTitleAsync(Title title)
        {
            _logger.Information(AppConstants.MethodStarted(nameof(GetGenreByTitleAsync)));

            Genre genre = await _genreRepositoryReadOnly.FirstOrDefaultAsync(genre => genre.Title.Equals(title));

            if (genre is null)
                return Result.Fail(NotFoundError.Entity(nameof(Genre)));

            return genre;
        }

        private async Task<Result<Genre>> CreateGenreByTitleAsync(Title title)
        {
            try
            {
                _logger.Information(AppConstants.MethodStarted(nameof(CreateGenreByTitleAsync)));

                Genre genre = new Genre(GenreId.New(), title);

                await _genreRepository.AddAsync(genre);

                await _unitOfWork.SaveChangesAsync();

                return genre;
            }
            catch (Exception ex)
            {
                _logger.Error(AppConstants.MethodFailed(nameof(CreateGenreByTitleAsync), ex.Message), ex, ex.InnerException);
                return Result.Fail(InternarServerError.New(AppConstants.MethodFailed(nameof(CreateGenreByTitleAsync), ex.Message)));
            }
        }

        #endregion
    }
}