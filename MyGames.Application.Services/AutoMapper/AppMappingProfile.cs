﻿using AutoMapper;

using MyGames.Domain.Core.GamesAggregate;
using MyGames.Domain.Core.GamesAggregate.Entities;
using MyGames.Domain.Dtos.ViewModels;

namespace MyGames.Application.Services.AutoMapper
{
    public class AppMappingProfile : Profile
    {
        public AppMappingProfile()
        {
            CreateMap<Game, GameViewModel>();
            CreateMap<Genre, GenreViewModel>();
            CreateMap<Studio, StudioViewModel>();
        }
    }
}