﻿namespace MyGames.Application.Services.Constants
{
    public static class AppConstants
    {
        public static string ERROR_CODE = "ErrorCode";

        public static string MethodStarted(string name)
        {
            return $"Method {name} started";
        }

        public static string MethodFailed(string name)
        {
            return $"Method {name} failed";
        }

        public static string MethodFailed(string name, string exception)
        {
            return $"Method {name} failed with exception {exception}";
        }

        public static string MethodEnded(string name)
        {
            return $"Method {name} ended";
        }
    }
}
