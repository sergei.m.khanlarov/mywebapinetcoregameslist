﻿using FluentResults;
using Serilog;
using System;
using System.Linq;

namespace MyGames.Application.Services.Extensions
{
    public static class ResultExtensions
    {
        public static Result LogError(this Result source, Exception ex, ILogger logger)
        {
            logger.Error(ex, source.Errors.FirstOrDefault().Message);

            return source;
        }

        public static Result LogWarning(this Result source, ILogger logger)
        {
            logger.Warning(source.Errors.FirstOrDefault().Message);

            return source;
        }
    }
}
