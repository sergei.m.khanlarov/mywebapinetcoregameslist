﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MyGames.Application.Services.AutoMapper;
using MyGames.Application.Services.Services;
using MyGames.Domain.Interfaces.Services;
using Serilog.Sinks.MSSqlServer;
using Serilog;

namespace MyGames.Application.Services
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<Serilog.ILogger>(new LoggerConfiguration()
                .WriteTo
                .MSSqlServer(
                    connectionString: configuration.GetConnectionString("DefaultConnection"),
                    sinkOptions: new MSSqlServerSinkOptions { 
                        TableName = "LogEvents",
                        AutoCreateSqlTable = true
                    })
                .CreateLogger());

            services.AddScoped<IGameServiceQuery, GameServiceQuery>();
            services.AddScoped<IGameServiceCommand, GameServiceCommand>();

            services.AddAutoMapper(typeof(AppMappingProfile));

            return services;
        }
    }
}