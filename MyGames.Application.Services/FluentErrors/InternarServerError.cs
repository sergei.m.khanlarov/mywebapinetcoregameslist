﻿using FluentResults;
using MyGames.Application.Services.Constants;

namespace MyGames.Application.Services.FluentErrors
{
    public class InternarServerError : Error
    {
        private InternarServerError(string message) : base(message)
        {
            Metadata.Add(AppConstants.ERROR_CODE, 500);
        }

        public static InternarServerError Default() 
        {
            return new InternarServerError($"Error occured!");
        }

        public static InternarServerError New(string message)
        {
            return new InternarServerError(message);
        }
    }
}
