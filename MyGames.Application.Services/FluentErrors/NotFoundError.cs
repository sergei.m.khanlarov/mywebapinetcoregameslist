﻿using FluentResults;
using MyGames.Application.Services.Constants;

namespace MyGames.Application.Services.FluentErrors
{
    public class NotFoundError : Error
    {
        private NotFoundError(string message) : base(message)
        {
            Metadata.Add(AppConstants.ERROR_CODE, 500);
        }

        public static NotFoundError Default()
        {
            return new NotFoundError($"Not found!");
        }

        public static NotFoundError Entity(string name)
        {
            return new NotFoundError($"{name} was not found!");
        }

        public static NotFoundError Entity(string name, string id)
        {
            return new NotFoundError($"{name} with id {id} was not found!");
        }


        public static NotFoundError New(string message)
        {
            return new NotFoundError(message);
        }
    }
}
