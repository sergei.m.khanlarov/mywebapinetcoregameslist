﻿using FluentResults;
using MyGames.Application.Services.Constants;

namespace MyGames.Application.Services.FluentErrors
{
    public class BadRequestError : Error
    {
        private BadRequestError(string message) : base(message)
        {
            Metadata.Add(AppConstants.ERROR_CODE, 400);
        }

        public static BadRequestError Default()
        {
            return new BadRequestError($"Incorrect input data!");
        }

        public static BadRequestError New(string message)
        {
            return new BadRequestError(message);
        }
    }
}
