﻿using System.Collections.Generic;

namespace MyGames.Application.Dtos
{
    public record CreateGameDto(string Title, string Studio, IReadOnlyCollection<string> Genres);

    public record UpdateGameDto(string Title, string Studio, IReadOnlyCollection<string> Genres);

    public record GetGamesDto(string Search, int? Offset, int? Limit);
}
