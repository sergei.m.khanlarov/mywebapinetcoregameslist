﻿using MyGames.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyGames.Domain.Interfaces.Repositories
{
    /// <summary>
    /// repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public interface IRepository<T, TId> where T : Entity<TId>
                                         where TId : IEquatable<TId>
    {
        /// <summary>
        /// Get first entity
        /// </summary>
        /// <param name="predicate">search params</param>
        /// <param name="properties">include property</param>
        /// <returns>Task<T></returns>
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate = default, IEnumerable<string>? properties = default);

        /// <summary>
        /// Get all queries
        /// </summary>
        /// <param name="properties">include property</param>
        /// <returns>Task<IEnumerable<T>></returns>
        public Task<IReadOnlyList<T>> GetAllAsync(IEnumerable<string>? properties = default);

        /// <summary>
        /// Find queries by predicate (where logic)
        /// </summary>
        /// <param name="predicate">Search predicate (LINQ)</param>
        /// <param name="properties">include property</param>
        /// <returns>Task<IEnumerable<T>></returns>
        public Task<IReadOnlyList<T>> FindByAsync(Expression<Func<T, bool>> predicate, IEnumerable<string>? properties = default);

        /// <summary>
        /// Add new entity
        /// </summary>
        /// <param name="entity">Entity object</param>
        public Task AddAsync(T entity);

        /// <summary>
        /// Remove entity from database
        /// </summary>
        /// <param name="entity">Entity object</param>
        public void Delete(T entity);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity">Entity object</param>
        public void Update(T entity);
    }
}