﻿using MyGames.Domain.Core.Base;
using MyGames.Domain.Interfaces.Repositories;
using System;

namespace MyGames.Domain.Interfaces.UnitOfWork
{
    public interface IUnitOfWorkQuery : IDisposable
    {
        /// <summary>
        /// Returns Repository
        /// </summary>
        /// <typeparam name="T">entity</typeparam>
        /// <typeparam name="TId">identifier</typeparam>
        /// <returns></returns>
        IRepositoryQuery<T, TId> ReadOnlyRepository<T, TId>() where T : Entity<TId>
                                                    where TId : IEquatable<TId>;
    }
}