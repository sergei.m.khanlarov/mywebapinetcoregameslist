﻿using MyGames.Domain.Core.Base;
using MyGames.Domain.Interfaces.Repositories;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MyGames.Domain.Interfaces.UnitOfWork
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Asynchronously saves all changes made in this unit of work to the database.
        /// </summary>
        public Task SaveChangesAsync(CancellationToken ct = default);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        public Task SaveChangesAndCommitTransactionAsync(CancellationToken ct = default);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        public Task BeginTransactionASync(CancellationToken ct = default);

        /// <summary>
        /// Asynchronously get repository
        /// </summary>
        /// <typeparam name="T">entity</typeparam>
        /// <typeparam name="TId">identifier</typeparam>
        /// <returns></returns>
        public IRepository<T, TId> Repository<T, TId>() where T : Entity<TId>
                                                        where TId : IEquatable<TId>;
    }
}