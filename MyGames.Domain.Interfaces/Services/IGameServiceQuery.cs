﻿using FluentResults;
using MyGames.Domain.Dtos.Queries;
using MyGames.Domain.Dtos.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyGames.Domain.Interfaces.Services
{
    public interface IGameServiceQuery
    {
        /// <summary>
        /// gets all games, acceptable by filter
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<Result<IReadOnlyCollection<GameViewModel>>> GetGamesAsync(GetGamesQuery query);

        /// <summary>
        /// gets an entity by id
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<Result<GameViewModel>> GetGameAsync(GetGameQuery query);
    }
}