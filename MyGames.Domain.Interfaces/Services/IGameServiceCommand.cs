﻿using FluentResults;
using MyGames.Domain.Dtos.Commands;
using MyGames.Domain.Dtos.ViewModels;
using System.Threading.Tasks;

namespace MyGames.Domain.Interfaces.Services
{
    public interface IGameServiceCommand
    {
        /// <summary>
        /// creates a game and saves it
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<Result<string>> CreateGameAsync(CreateGameCommand command);

        /// <summary>
        /// updates games
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<Result<GameViewModel>> UpdateGameAsync(UpdateGameCommand command);

        /// <summary>
        /// deletes a game
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<Result> DeleteGameAsync(DeleteGameCommand command);
    }
}