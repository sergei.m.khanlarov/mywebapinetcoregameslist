﻿namespace MyGames.Domain.Core.Base
{
    /// <summary>
    /// Interface that identify the aggregate
    /// </summary>
    public interface IAggregateRoot { }
}