﻿using System;
using System.Collections.Generic;

namespace MyGames.Domain.Core.Base
{
    /// <summary>
    /// Entity class
    /// </summary>
    /// <typeparam name="TId">id</typeparam>
    public abstract class Entity<TId> where TId : IEquatable<TId>
    {
        protected Entity() { }

        protected Entity(TId id)
        {
            Id = id;
            CreatedAt = DateTime.Now;
            CreatedBy = Environment.UserName;
            ModifiedAt = CreatedAt;
            ModifiedBy = Environment.UserName;
        }

        public TId Id { get; }

        public DateTime CreatedAt { get; }

        public string CreatedBy { get; }

        public DateTime ModifiedAt { get; protected set; }

        public string ModifiedBy { get; protected set; }

        public bool IsDeleted { get; protected set; }

        private bool IsTransient() => Id.Equals(default);

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(this, obj))
                return true;

            if (obj is not Entity<TId> item)
                return false;

            if (GetType() != item.GetType())
                return false;

            if (item.IsTransient() || IsTransient())
                return false;

            return item.Id.Equals(Id);
        }

        public override int GetHashCode() => EqualityComparer<TId>.Default.GetHashCode(Id);

        public static bool operator ==(Entity<TId>? left, Entity<TId>? right) => left?.Equals(right) ?? Equals(right, null);

        public static bool operator !=(Entity<TId> left, Entity<TId> right) => !(left == right);

        public bool Delete()
        {
            if (IsDeleted)
                return true;

            IsDeleted = true;

            ModifiedAt = DateTime.Now;
            ModifiedBy = Environment.UserName;

            return true;
        }
    }
}