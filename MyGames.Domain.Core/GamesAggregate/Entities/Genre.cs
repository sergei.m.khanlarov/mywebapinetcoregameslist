﻿#nullable enable
using MyGames.Domain.Core.Base;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;
using System.Collections.Generic;

namespace MyGames.Domain.Core.GamesAggregate.Entities
{
    /// <summary>
    /// Genre of game
    /// </summary>
    public class Genre : Entity<GenreId>
    {
        /// <summary>
        /// Title of genre
        /// </summary>
        public Title Title { get; private set; }

        //ef core
        public IReadOnlyCollection<Game> Games { get; private set; }

        //ef core
        private Genre() { }

        /// <summary>
        /// Creates a genre
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        public Genre(GenreId id, Title title) : base(id) => Title = title;
    }
}