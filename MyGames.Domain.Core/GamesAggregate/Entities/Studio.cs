﻿#nullable enable
using MyGames.Domain.Core.Base;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;
using System.Collections.Generic;

namespace MyGames.Domain.Core.GamesAggregate.Entities
{
    /// <summary>
    /// Studio
    /// </summary>
    public class Studio : Entity<StudioId>
    {
        /// <summary>
        /// Title of studio
        /// </summary>
        public Title Title { get; private set; }

        //ef core
        public IReadOnlyCollection<Game> Games { get; private set; }

        //ef core
        private Studio() { }

        /// <summary>
        /// Creates a studio
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        public Studio(StudioId id, Title title) : base(id) => Title = title;
    }
}