﻿using MyGames.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyGames.Domain.Core.GamesAggregate.ValueObjects
{
    /// <summary>
    /// Class id of genre
    /// </summary>
    public class GenreId : ValueObject, IEquatable<GenreId>
    {
        private readonly Guid _guid;
        private readonly string _className;

        //ef core
        private GenreId() { }

        private GenreId(Guid guid)
        {
            _guid = guid;
            _className = nameof(GenreId);
        }

        /// <summary>
        /// Creates new Genre id by Guid id
        /// </summary>
        /// <param name="guid">guid</param>
        /// <returns>Genre Id object</returns>
        public static GenreId New(Guid guid) => new(guid);


        /// <summary>
        /// Creates new id
        /// </summary>
        /// <returns>GenreId Id object</returns>
        public static GenreId New() => new(Guid.NewGuid());

        /// <summary>
        /// Parse string to studio id value
        /// </summary>
        /// <param name="id">string id</param>
        /// <returns>Genre Id object</returns>
        /// <exception cref="ArgumentNullException">if id is null</exception>
        /// <exception cref="ArgumentException">if id is not acceptable</exception>
        public static GenreId Parse(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            string[] idTmp = id.Split('_');

            if (idTmp.Length != 2 ||
                !idTmp.First().Equals(nameof(GenreId)) ||
                !Guid.TryParse(idTmp.Last(), out Guid guid))
            {
                throw new ArgumentException($"value {id} is StudioIdId");
            }

            return new GenreId(guid);
        }

        /// <summary>
        /// Override, returns Genre Id string
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{_className}_{_guid.ToString()}";

        /// <summary>
        /// Compare Value objects
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _guid;
            yield return _className;
        }

        public bool Equals(GenreId other)
        {
            if (ReferenceEquals(null, other))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return base.Equals(other) && _guid.Equals(other._guid) && _className == other._className;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((GenreId)obj);
        }

        public override int GetHashCode()
            => HashCode.Combine(base.GetHashCode(), _guid, _className);
    }
}