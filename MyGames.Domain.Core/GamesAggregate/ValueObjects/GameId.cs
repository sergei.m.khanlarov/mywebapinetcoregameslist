﻿using MyGames.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyGames.Domain.Core.GamesAggregate.ValueObjects
{
    /// <summary>
    /// Class id of game
    /// </summary>
    public class GameId : ValueObject, IEquatable<GameId>
    {
        private readonly Guid _guid;
        private readonly string _className;

        //ef core
        private GameId() { }

        private GameId(Guid guid)
        {
            _guid = guid;
            _className = nameof(GameId);
        }

        /// <summary>
        /// Creates new Game id by Guid id
        /// </summary>
        /// <param name="guid">guid</param>
        /// <returns>Game Id object</returns>
        public static GameId New(Guid guid) => new(guid);

        /// <summary>
        /// Creates new id
        /// </summary>
        /// <returns>GameId Id object</returns>
        public static GameId New() => new(Guid.NewGuid());

        /// <summary>
        /// Parse string to studio id value
        /// </summary>
        /// <param name="id">string id</param>
        /// <returns>Game Id object</returns>
        /// <exception cref="ArgumentNullException">if id is null</exception>
        /// <exception cref="ArgumentException">if id is not acceptable</exception>
        public static GameId Parse(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            string[] idTmp = id.Split('_');

            if (idTmp.Length != 2 ||
                !idTmp.First().Equals(nameof(GameId)) ||
                !Guid.TryParse(idTmp.Last(), out Guid guid))
            {
                throw new ArgumentException($"value {id} is StudioIdId");
            }

            return new GameId(guid);
        }

        /// <summary>
        /// Override, returns Game Id string
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{_className}_{_guid.ToString()}";

        /// <summary>
        /// Compare Value objects
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _guid;
            yield return _className;
        }

        public bool Equals(GameId other)
        {
            if (ReferenceEquals(null, other))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return base.Equals(other) && _guid.Equals(other._guid) && _className == other._className;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == this.GetType() && Equals((GameId)obj);
        }

        public override int GetHashCode()
            => HashCode.Combine(base.GetHashCode(), _guid, _className);
    }
}