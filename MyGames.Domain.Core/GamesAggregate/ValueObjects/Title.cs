﻿using MyGames.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace MyGames.Domain.Core.GamesAggregate.ValueObjects
{
    /// <summary>
    /// Game title value object
    /// </summary>
    public class Title : ValueObject
    {
        private readonly string _title;

        public string NormalizedName => _title.Normalize().ToUpper();

        //ef core
        private Title() { }


        private Title(string title)
            => _title = title.Trim();

        /// <summary>
        /// Creates a Title
        /// </summary>
        /// <param name="value">title</param>
        /// <returns>Title value object</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static Title Parse(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));

            return new Title(value);
        }

        /// <summary>
        /// Override, returns Title string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => _title;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _title;
        }
    }
}