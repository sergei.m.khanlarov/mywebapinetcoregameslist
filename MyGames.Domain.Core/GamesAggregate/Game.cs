﻿using MyGames.Domain.Core.Base;
using MyGames.Domain.Core.GamesAggregate.Entities;
using MyGames.Domain.Core.GamesAggregate.ValueObjects;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace MyGames.Domain.Core.GamesAggregate
{
    /// <summary>
    /// Game
    /// </summary>
    public class Game : Entity<GameId>, IAggregateRoot
    {
        private readonly ISet<Genre> _genres;

        /// <summary>
        /// Title of the game
        /// </summary>
        public Title Title { get; private set; }

        /// <summary>
        /// Studio of the game
        /// </summary>
        public Studio Studio { get; private set; }

        /// <summary>
        /// Games' ids, games produced by this genre
        /// </summary>
        public IReadOnlyCollection<Genre> Genres => _genres.ToImmutableList();

        //ef core
        private Game() { }

        /// <summary>
        /// Creates a genre
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="genres"></param>
        /// <param name="studio"></param>
        public Game(GameId id,
                    Title title,
                    ISet<Genre> genres,
                    Studio studio) : base(id)
        {
            Title = title;
            _genres = genres ?? new HashSet<Genre>();
            Studio = studio;
        }

        /// <summary>
        /// Updates Game info
        /// </summary>
        /// <param name="title">Game title</param>
        /// <param name="genres">game genres</param>
        /// <param name="studio">game studio</param>
        public void Update(Title title, IEnumerable<Genre> genres, Studio studio)
        {
            if (!Title.Equals(title))
                Title = title;

            _genres.Clear();

            if (genres is not null && genres.Any())
                foreach (Genre genre in genres)
                    _genres.Add(genre);

            if (!Studio.Equals(studio))
                Studio = studio;

            ModifiedAt = DateTime.Now;
            ModifiedBy = Environment.UserName;
        }
    }
}