﻿using FluentResults;
using MyGames.Application.Dtos;
using MyGames.Domain.Dtos.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyGames.Application.Interfaces
{
    public interface IGameServiceBuilder
    {
        /// <summary>
        /// creates a game
        /// </summary>
        /// <param name="command">команда с данными</param>
        /// <returns></returns>
        public Task<Result<string>> CreateGameAsync(CreateGameDto command);

        /// <summary>
        /// updates a game
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<Result<GameViewModel>> UpdateGameAsync(string gameId, UpdateGameDto command);

        /// <summary>
        /// deletes a game
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public Task<Result> DeleteGameAsync(string gameId);

        /// <summary>
        /// gets games info
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<Result<IReadOnlyCollection<GameViewModel>>> GetGamesAsync(GetGamesDto query);

        /// <summary>
        /// gets games info
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public Task<Result<GameViewModel>> GetGameAsync(string gameId);
    }
}
