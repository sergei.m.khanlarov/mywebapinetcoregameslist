﻿using FluentResults;
using FluentResults.Extensions.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using MyGames.Application.Services.Constants;
using System.Linq;

namespace MyGames.Api.Services.CustomResultProfile
{
    public class CustomResultProfile : DefaultAspNetCoreResultEndpointProfile
    {
        public override ActionResult TransformFailedResultToActionResult(FailedResultToActionResultTransformationContext context)
        {
            ResultBase result = context.Result;

            var error = result.Errors.FirstOrDefault();

            if (!error.Metadata.ContainsKey(AppConstants.ERROR_CODE))
                return new BadRequestObjectResult(result.Errors);

            switch (error.Metadata[AppConstants.ERROR_CODE])
            {
                case 404:
                    return new NotFoundObjectResult(result.Errors);
                case 500: 
                    return new InternalServerErrorObjectResult(result.Errors);
            }

            return new BadRequestObjectResult(result.Errors);
        }

        public override ActionResult TransformOkNoValueResultToActionResult(OkResultToActionResultTransformationContext<Result> context)
        {
            return new NoContentResult();
        }
    }
}
