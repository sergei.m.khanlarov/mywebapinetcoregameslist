﻿using FluentResults;
using MyGames.Application.Dtos;
using MyGames.Application.Interfaces;
using MyGames.Application.Services.FluentErrors;
using MyGames.Domain.Dtos.Commands;
using MyGames.Domain.Dtos.Queries;
using MyGames.Domain.Dtos.ViewModels;
using MyGames.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyGames.Api.Services.Services
{
    /// <summary>
    /// builder class
    /// </summary>
    public class GamesServiceBuilder : IGameServiceBuilder
    {
        private readonly IGameServiceCommand _command;
        private readonly IGameServiceQuery _query;

        public GamesServiceBuilder(IGameServiceQuery query, IGameServiceCommand command)
        {
            _query = query;
            _command = command;
        }

        async Task<Result<string>> IGameServiceBuilder.CreateGameAsync(CreateGameDto command)
        {
            try
            {
                return await _command.CreateGameAsync(new CreateGameCommand(command.Title, command.Studio, command.Genres));
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError.New(ex.Message));
            }
        }

        async Task<Result<GameViewModel>> IGameServiceBuilder.UpdateGameAsync(string gameId, UpdateGameDto command)
        {
            try
            {
                return await _command.UpdateGameAsync(new UpdateGameCommand(gameId, command.Title, command.Studio, command.Genres));
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError.New(ex.Message));
            }
        }

        async Task<Result> IGameServiceBuilder.DeleteGameAsync(string gameId)
        {
            try
            {
                return await _command.DeleteGameAsync(new DeleteGameCommand(gameId));
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError.New(ex.Message));
            }
        }

        async Task<Result<IReadOnlyCollection<GameViewModel>>> IGameServiceBuilder.GetGamesAsync(GetGamesDto query)
        {
            try
            {
                return await _query.GetGamesAsync(new GetGamesQuery(query.Search, query.Offset, query.Limit));
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError.New(ex.Message));
            }
        }

        async Task<Result<GameViewModel>> IGameServiceBuilder.GetGameAsync(string gameId)
        {
            try
            {
                return await _query.GetGameAsync(new GetGameQuery(gameId));
            }
            catch (Exception ex)
            {
                return Result.Fail(InternarServerError.New(ex.Message));
            }
        }
    }
}