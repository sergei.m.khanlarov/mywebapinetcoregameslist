﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

using MyGames.Api.Services.Services;
using MyGames.Application.Interfaces;
using MyGames.Application.Services;
using MyGames.Infrastructure.Data;

namespace MyGames.Api.Services
{
    public static class ServiceSettings
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.TryAddTransient<IGameServiceBuilder, GamesServiceBuilder>();
            services.AddInfrastructure(configuration);
            services.AddApplication(configuration);

            return services;
        }
    }
}