﻿namespace MyGames.Domain.Dtos.Queries
{
    public record GetGamesQuery(string Search, int? Offset, int? Limit);

    public record GetGameQuery(string Id);
}
