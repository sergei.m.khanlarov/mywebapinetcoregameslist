﻿using System.Collections.Generic;

namespace MyGames.Domain.Dtos.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="Title"></param>
    /// <param name="Studio"></param>
    /// <param name="Genres"></param>
    public record GameViewModel(string Id,
                                string Title,
                                StudioViewModel Studio,
                                IReadOnlyCollection<GenreViewModel> Genres);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="Title"></param>
    public record StudioViewModel(string Id, string Title);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="Title"></param>
    public record GenreViewModel(string Id, string Title);
}