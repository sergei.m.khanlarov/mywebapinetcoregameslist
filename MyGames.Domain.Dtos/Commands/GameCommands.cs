﻿using System.Collections.Generic;

namespace MyGames.Domain.Dtos.Commands
{
    public record CreateGameCommand(string Title, string Studio, IReadOnlyCollection<string> Genres);

    public record UpdateGameCommand(string Id, string Title, string Studio, IReadOnlyCollection<string> Genres);

    public record DeleteGameCommand(string Id);
}
