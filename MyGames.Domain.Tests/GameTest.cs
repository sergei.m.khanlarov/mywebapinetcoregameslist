﻿using NUnit.Framework;

namespace MyGames.Domain.Tests
{
    [TestFixture]
    [Author("Sergei Khanlarov", "sergei.m.khanlarov@gmail.com")]
    [Description("Tests for game aggregate")]
    public class GameTest
    {
        [Test]
        public void Game_New_Ok() { Assert.Pass(); }

        [Test]
        public void Game_New_Fail() { Assert.Pass(); }

        [Test]
        public void Game_Update_Ok() { Assert.Pass(); }

        [Test]
        public void Game_Update_Fail() { Assert.Pass(); }

        [Test]
        public void Genre_New_Ok() { Assert.Pass(); }

        [Test]
        public void Genre_New_Fail() { Assert.Pass(); }

        [Test]
        public void Genre_AddGame_Ok() { Assert.Pass(); }

        [Test]
        public void Genre_AddGame_Fail() { Assert.Pass(); }

        [Test]
        public void Studio_New_Ok() { Assert.Pass(); }

        [Test]
        public void Studio_New_Fail() { Assert.Pass(); }
    }
}